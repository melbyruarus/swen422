//
//  Visualisation.h
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#ifndef __SWEN422_project__Visualisation__
#define __SWEN422_project__Visualisation__

#include "VisualisationData.h"
#include <ANN/ANN.h>
#include "Node.h"
#include <vector>

namespace swen422 {
	namespace AxisScale {
		enum Type {
			Linear = 0,
			Logarithmic = 1
		};
	}
	
	namespace AxisOption {
		enum Type {
			PositionX = 0,
			PositionY = 1,
			PositionZ = 2,
			Radius = 3,
			Magnitude = 4,
			Luminosity = 5,
			ColorR = 6,
			ColorG = 7,
			ColorB = 8,
			Temperature = 9
		};
	}
	
	namespace ColorOption {
		enum Type {
			Position = 0,
			Radius = 1,
			Magnitude = 2,
			Luminosity = 3,
			Color = 4,
			Temperature = 5
		};
	}
	
	class Visualisation {
	private:
		static const int kOctreeSize = 256;
		
		VisualisationData *_backing;
		std::vector<Node *> _allNodes;
		ANNkd_tree *_kdtree = NULL;
		ANNpointArray _kdtreeNodes = NULL;
		
		AxisOption::Type _xAxis;
		AxisOption::Type _yAxis;
		AxisOption::Type _zAxis;
		AxisOption::Type _sizeAxis;
		ColorOption::Type _colorAxis;
		
		AxisScale::Type _xAxisScale;
		AxisScale::Type _yAxisScale;
		AxisScale::Type _zAxisScale;
		AxisScale::Type _sizeAxisScale;
		AxisScale::Type _colorAxisScale;
		
		void updateNodes();
		float valueForOption(AxisOption::Type option, const Star *s);
		glm::vec3 valueForOption(ColorOption::Type option, const Star *s);
		glm::vec3 colorify(const glm::vec3 &color);
		float scaleify(float value, AxisScale::Type scale);
		glm::vec3 scaleify(glm::vec3 value, AxisScale::Type scale);
	public:
		constexpr static const float kGridSize = 1.0f/kOctreeSize;
		
		Visualisation(VisualisationData *visualisationData);
		~Visualisation();
		
		const std::vector<Node *> &nodes() const { return _allNodes; }
		const Node *nodeWithName(const std::string &name) const;
		
		void resetToDefault();
		void getClosestNodes(const glm::vec3 &pos, int count, std::vector<Node *> &nodes, std::vector<float> &distances);
		
		void setXAxis(AxisOption::Type value) { _xAxis = value; updateNodes(); }
		void setYAxis(AxisOption::Type value) { _yAxis = value; updateNodes(); }
		void setZAxis(AxisOption::Type value) { _zAxis = value; updateNodes(); }
		void setSizeAxis(AxisOption::Type value) { _sizeAxis = value; updateNodes(); }
		void setColorAxis(ColorOption::Type value) { _colorAxis = value; updateNodes(); }
		
		void setXAxisScale(AxisScale::Type value) { _xAxisScale = value; updateNodes(); }
		void setYAxisScale(AxisScale::Type value) { _yAxisScale = value; updateNodes(); }
		void setZAxisScale(AxisScale::Type value) { _zAxisScale = value; updateNodes(); }
		void setSizeAxisScale(AxisScale::Type value) { _sizeAxisScale = value; updateNodes(); }
		void setColorAxisScale(AxisScale::Type value) { _colorAxisScale = value; updateNodes(); }
		
		AxisOption::Type xAxis() { return _xAxis; }
		AxisOption::Type yAxis() { return _yAxis; }
		AxisOption::Type zAxis() { return _zAxis; }
		AxisOption::Type sizeAxis() { return _sizeAxis; }
		ColorOption::Type colorAxis() { return _colorAxis; }
		
		AxisScale::Type xAxisScale() { return _xAxisScale; }
		AxisScale::Type yAxisScale() { return _yAxisScale; }
		AxisScale::Type zAxisScale() { return _zAxisScale; }
		AxisScale::Type sizeAxisScale() { return _sizeAxisScale; }
		AxisScale::Type colorAxisScale() { return _colorAxisScale; }
	};
}

#endif /* defined(__SWEN422_project__Visualisation__) */
