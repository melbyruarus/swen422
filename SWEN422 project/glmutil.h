//
//  glmutil.h
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#ifndef SWEN422_project_glmutil_h
#define SWEN422_project_glmutil_h

#include <glm/glm.hpp>
#include <ostream>

template<typename T, typename M>
static glm::detail::tvec3<T> convert(const M &eigenvec) {
	return glm::detail::tvec3<T>((float)eigenvec(0), (float)eigenvec(1), (float)eigenvec(2));
}

#endif
