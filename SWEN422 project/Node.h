//
//  Node.h
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#ifndef __SWEN422_project__Node__
#define __SWEN422_project__Node__

#include "Star.h"
#include <glm/glm.hpp>

namespace swen422 {
	struct Node {
		const Star *star;
		glm::vec3 color;
		glm::vec3 position;
		float size;
	};
}

#endif /* defined(__SWEN422_project__Node__) */
