//
//  VisualisationRenderer.h
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#ifndef __SWEN422_project__VisualisationRenderer__
#define __SWEN422_project__VisualisationRenderer__

#include "Visualisation.h"
#include <ILGL/Presentation/VBO.h>
#include <ILGL/Presentation/Renderer.h>
#include <ILGL/Presentation/Shader.h>
#include <ILGL/Presentation/MeshRenderer.h>
#include <ILGL/Resources/ResourcePointer.h>
#include <Leap.h>
#include <glm/gtx/quaternion.hpp>

using namespace ilgl::presentation;
using namespace ilgl::resources;

namespace swen422 {
	struct Hand {
		// Attribtues
		glm::vec3 position;
		glm::vec3 frameVelocity;
		ilgl::timestamp_t lastUpdate;
		float lastFrameLength;
		bool isClosed;
		
		// Intial state info
		glm::dmat4 modelViewMatrix;
		glm::vec3 cameraPosition;
		
		// Gesture info
		glm::vec3 gestureStartPosition;
		
		Hand(int32_t id): _id(id) {}
		
		int32_t id() const { return _id; }
	private:
		int32_t _id;
	};
	
	class VisualisationRenderer : public Leap::Listener {
		// Constants
	public:
		constexpr static const float kFPS = 30;
	private:
		constexpr static const float kWorldScaleFactor = 100;
		constexpr static const float kZNear = 0.01f;
		constexpr static const float kZFar = kWorldScaleFactor*2;
		constexpr static const float kDampningFactor = 0.15;
		static const float kFrameDampningFactor;
		constexpr static const float kVelocityLowerThreshold = 0.001;
		constexpr static const float kVelocityUpperThreshold = 0.1;
		constexpr static const float kRotationAngleLowerThreshold = 0.1;
		static const float kRotationAngleLowerInvCosThreshold;
		
		// Leap motion
		Leap::Controller _leapController;
		Leap::Screen _mainScreen;
		
		// View info
		glm::vec2 _viewOrigin;
		glm::vec2 _viewSize;
		glm::dmat4 _projectionMatrix;
		glm::dmat4 _modelViewMatrix;
		bool _displayLabels = true;
		
		// Resources
		ResourceDirectory *_resourceDir = NULL;
		Visualisation *_visualisation = NULL;
		
		// OpenGL state
		Renderer *_renderer = NULL;
		VBO *_pointVbo = NULL;
		VBO *_colorAndSizeVbo = NULL;
		VBO *_oldPointVbo = NULL;
		VBO *_oldColorAndSizeVbo = NULL;
		VBO *_starIndicies = NULL;
		std::vector<float> _oldPoints;
		std::vector<float> _oldColorsAndSize;
		
		ShaderProgram *_pointShader = NULL;
		ShaderProgram *_quadShader = NULL;
		ShaderProgramUniformLocation *_trasitioningPercentageUniformLocation = NULL;
		MeshRenderer *_quad = NULL;
		MeshRenderer *_sphere = NULL;
		bool _needVBOUpdate = true;
		
		// Camera
		glm::vec3 _cameraCenter;
		float _cameraDistance;
		glm::dquat _cameraRotation;
		
		// Gesture recognition
		std::vector<Hand> _hands;
		std::vector<Hand> _lastFrameHands;
		glm::vec3 _startGestureCameraCenter;
		float _startGestureCameraDistance;
		glm::dquat _startGestureCameraRotation;
		glm::dmat4 _startGestureCameraMatrix;
		glm::vec3 _translationVelocity = glm::vec3(0);
		int _closedHandsLastFrame = 0;
		
		bool _momentuming = false;
		glm::vec3 _momentumVelocity;
		
		// Transition
		bool _transitioning = false;
		int _transitionFrame = 0;
		
		// User testing
		const Node *_selectedStar = NULL;
		
		// Methods
		
		// OpenGL
		void setupVBO();
		void updateModelView();
		glm::dmat4 cameraMatrix() const;
		glm::vec3 cameraPosition() const;
		glm::dmat4 modelViewMatrix() const;
		void applyPerspectiveMatrix() const;
		void applyOrthoMatrix() const;
		void glInit();
		void drawQuad(double x, double y, double z, double w, double h);
		void getVBOData(std::vector<float> &points, std::vector<float> &colorsAndSize);
		glm::dvec4 project(glm::dvec4 point, bool *isVisible=NULL) const;
		glm::ivec2 ndcToScreen(glm::dvec4 ndc) const;
		
		// Gestures
		void processGesture();
		
		// Leap callbacks
		virtual void onConnect(const Leap::Controller &controller);
		virtual void onFrame(const Leap::Controller& controller);
	public:		
		VisualisationRenderer();
		~VisualisationRenderer();
		
		void setResourceDir(ResourceDirectory *dir) { _resourceDir = dir; }
		void setVisualisation(Visualisation *visualisation) { _visualisation = visualisation; }
		
		void updateProjection(const int &width, const int &height);
		void render();
		
		void setViewOrigin(const glm::vec2 &origin) { _viewOrigin = origin; }
		
		void prepareVisualisationUpdate();
		void updateVisualisation();
		
		void resetViewport();
		
		void setHighlightedStar(int o);
		void setSetDisplayLabels(bool display);
	};
}

#endif /* defined(__SWEN422_project__VisualisationRenderer__) */
