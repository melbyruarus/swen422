//
//  CelestiaInterface.h
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#ifndef __SWEN422_project__CelestiaInterface__
#define __SWEN422_project__CelestiaInterface__

#include <string>
#include "VisualisationLoader.h"

class CelestiaInterface {
private:
	std::string _resourceDir;
public:
	CelestiaInterface(std::string resourceDir): _resourceDir(resourceDir) {}
	
	bool loadDatabase(swen422::VisualisationLoader *loader);
};

#endif /* defined(__SWEN422_project__CelestiaInterface__) */
