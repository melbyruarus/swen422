//
//  ILOpenGLView.h
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include "VisualisationRenderer.h"

@interface ILOpenGLView : NSOpenGLView {
	swen422::VisualisationRenderer *_renderer;
}

@property (assign) swen422::VisualisationRenderer *renderer;

@end
