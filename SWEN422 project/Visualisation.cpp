//
//  Visualisation.cpp
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#include "Visualisation.h"
#include <ILGL/Common/Logging.h>


namespace swen422 {
	Visualisation::Visualisation(VisualisationData *visualisationData):
	_backing(visualisationData) {
		for(const Star *s : _backing->stars()) {
			Node *n = new Node();
			n->star = s;
			
			_allNodes.push_back(n);
		}
	}
	
	Visualisation::~Visualisation() {
		for(Node *n : _allNodes) {
			delete n;
		}
		
		delete _kdtree;
	}
	
	void Visualisation::resetToDefault() {
		_xAxis = AxisOption::PositionX;
		_yAxis = AxisOption::PositionY;
		_zAxis = AxisOption::PositionZ;
		_sizeAxis = AxisOption::Radius;
		_colorAxis = ColorOption::Color;
		
		_xAxisScale = AxisScale::Linear;
		_yAxisScale = AxisScale::Linear;
		_zAxisScale = AxisScale::Linear;
		_sizeAxisScale = AxisScale::Linear;
		_colorAxisScale = AxisScale::Linear;
		
		updateNodes();
	}
	
	void Visualisation::getClosestNodes(const glm::vec3 &pos, int count, std::vector<Node *> &nodes, std::vector<float> &distances) {
		ANNpoint queryPt = annAllocPt(3);
		queryPt[0] = pos.x;
		queryPt[1] = pos.y;
		queryPt[2] = pos.z;
				
		ANNidxArray nnIdx = new ANNidx[count];
		ANNdistArray dists = new ANNdist[count];
		
		_kdtree->annkSearch(queryPt,
						   count,
						   nnIdx,
						   dists,
						   0);
		
		for(int i=0;i<count;i++) {
			nodes.push_back(_allNodes[nnIdx[i]]);
			distances.push_back(sqrt(dists[i]));
		}
				
		annDeallocPt(queryPt);
		delete [] nnIdx;
		delete [] dists;
	}
	
	glm::vec3 Visualisation::colorify(const glm::vec3 &color) {
		return color * 0.8f + glm::vec3(0.2);
	}
	
	glm::vec3 Visualisation::valueForOption(ColorOption::Type option, const Star *s) {
		switch(option) {
			case ColorOption::Position:
				return _backing->normalizePosition(s->position);
			case ColorOption::Magnitude:
				return glm::vec3(_backing->normalizeAbsoluteMagnitude(s->absoluteMagnitude));
			case ColorOption::Radius:
				return glm::vec3(_backing->normalizeRadius(s->radius));
			case ColorOption::Luminosity:
				return glm::vec3(_backing->normalizeLuminosity(s->luminosity));
			case ColorOption::Temperature:
				return glm::vec3(_backing->normalizeTemperature(s->temperature));
			case ColorOption::Color:
				return _backing->normalizeColor(s->apparentColor);
		}
	}
	
	static float logConversion(const float &v) {
		assert(v >= 0);
		assert(v <= 1);
		float t = log10f(1+9*log10f(1+9*v));
		assert(t >= 0);
		assert(t <= 1);
		
		return t;
	}
	
	float Visualisation::valueForOption(AxisOption::Type option, const Star *s) {
		switch(option) {
			case AxisOption::PositionX:
				return _backing->normalizePositionX(s->position.x);
			case AxisOption::PositionY:
				return _backing->normalizePositionY(s->position.y);
			case AxisOption::PositionZ:
				return _backing->normalizePositionZ(s->position.z);
			case AxisOption::Magnitude:
				return _backing->normalizeAbsoluteMagnitude(s->absoluteMagnitude);
			case AxisOption::Radius:
				return _backing->normalizeRadius(s->radius);
			case AxisOption::Luminosity:
				return _backing->normalizeLuminosity(s->luminosity);
			case AxisOption::Temperature:
				return _backing->normalizeTemperature(s->temperature);
			case AxisOption::ColorR:
				return _backing->normalizeColorR(s->apparentColor.r);
			case AxisOption::ColorG:
				return _backing->normalizeColorG(s->apparentColor.g);
			case AxisOption::ColorB:
				return _backing->normalizeColorB(s->apparentColor.b);
		}
	}
	
	void Visualisation::updateNodes() {
		std::clock_t start = std::clock();
		
		delete _kdtree;
		if(_kdtreeNodes) {
			annDeallocPts(_kdtreeNodes);
		}
		_kdtreeNodes = annAllocPts(_allNodes.size(), 3);
		
		int count=0;
		for(Node *n : _allNodes) {
			n->position.x = scaleify(valueForOption(_xAxis, n->star), _xAxisScale);
			n->position.y = scaleify(valueForOption(_yAxis, n->star), _yAxisScale);
			n->position.z = scaleify(valueForOption(_zAxis, n->star), _zAxisScale);
			n->size = scaleify(valueForOption(_sizeAxis, n->star), _sizeAxisScale);
			n->color = scaleify(colorify(valueForOption(_colorAxis, n->star)), _colorAxisScale);
			
			_kdtreeNodes[count][0] = n->position.x;
			_kdtreeNodes[count][1] = n->position.y;
			_kdtreeNodes[count][2] = n->position.z;
			count++;
		}
		
		_kdtree = new ANNkd_tree(_kdtreeNodes, _allNodes.size(), 3);
		
		ILLogd("Updating datastructures took: %f seconds", (float)(std::clock() - start)/(float)CLOCKS_PER_SEC);
	}
	
	float Visualisation::scaleify(float value, AxisScale::Type scale) {
		switch(scale) {
			case AxisScale::Linear:
				return value;
			case AxisScale::Logarithmic:
				return logConversion(value);
		}
	}
	
	glm::vec3 Visualisation::scaleify(glm::vec3 value, AxisScale::Type scale) {
		switch(scale) {
			case AxisScale::Linear:
				return value;
			case AxisScale::Logarithmic:
				return glm::vec3(logConversion(value.x),
								 logConversion(value.y),
								 logConversion(value.z));
		}
	}
	
	const Node *Visualisation::nodeWithName(const std::string &name) const {
		for(const Node *n : _allNodes) {
			if(n->star->name == name) {
				return n;
			}
		}
		
		return NULL;
	}
}