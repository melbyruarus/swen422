//
//  Star.h
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#ifndef __SWEN422_project__Star__
#define __SWEN422_project__Star__

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "glmutil.h"
#include <string>
#include <ostream>

namespace swen422 {
	enum StarType
    {
		NormalStar     = 0,
		WhiteDwarf     = 1,
		NeutronStar    = 2,
        BlackHole      = 3,
    };
	
    enum SpectralClass
    {
		Spectral_O     = 0,
		Spectral_B     = 1,
		Spectral_A     = 2,
		Spectral_F     = 3,
		Spectral_G     = 4,
		Spectral_K     = 5,
		Spectral_M     = 6,
		Spectral_R     = 7, // superceded by class C
		Spectral_S     = 8,
		Spectral_N     = 9, // superceded by class C
		Spectral_WC    = 10,
		Spectral_WN    = 11,
        Spectral_Unknown = 12,
        Spectral_L     = 13,
        Spectral_T     = 14,
        Spectral_C     = 15,
        Spectral_DA    = 16, // white dwarf A (Balmer lines, no He I or metals)
        Spectral_DB    = 17, // white dwarf B (He I lines, no H or metals)
        Spectral_DC    = 18, // white dwarf C, continuous spectrum
        Spectral_DO    = 19, // white dwarf O, He II strong, He I or H
        Spectral_DQ    = 20, // white dwarf Q, carbon features
        Spectral_DZ    = 21, // white dwarf Z, metal lines only, no H or He
        Spectral_D     = 22, // generic white dwarf, no additional data
        Spectral_DX    = 23,
        Spectral_Count = 24,
    };
	
    enum LuminosityClass
    {
		Lum_Ia0     = 0,
		Lum_Ia      = 1,
		Lum_Ib      = 2,
		Lum_II      = 3,
		Lum_III     = 4,
		Lum_IV      = 5,
		Lum_V       = 6,
		Lum_VI      = 7,
        Lum_Unknown = 8,
        Lum_Count   = 9,
    };
	
	struct Star {
		glm::vec3 position;
		float absoluteMagnitude;
		float bolometricMagnitude;
		float luminosity;
		float radius;
		float temperature;
		std::string spectralTypeName;
		SpectralClass spectralClass;
		int subclass;
		LuminosityClass luminosityClass;
		StarType type;
		glm::vec3 apparentColor;
		
		std::string name;
	};
	
	std::ostream &operator<<(std::ostream &out, const Star &s);
}

#endif /* defined(__SWEN422_project__Star__) */
