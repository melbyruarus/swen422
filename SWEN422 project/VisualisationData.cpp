//
//  VisualisationData.cpp
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#include "VisualisationData.h"

namespace swen422 {
	static void updateMaxMin(float &max, float &min, const float value) {
		max = std::max(max, value);
		min = std::min(min, value);
	}
	
	static void updateMaxMin(glm::vec3 &max, glm::vec3 &min, const glm::vec3 &value) {
		updateMaxMin(max.x, min.x, value.x);
		updateMaxMin(max.y, min.y, value.y);
		updateMaxMin(max.z, min.z, value.z);
	}
	
	void VisualisationData::addStar(const Star *star) {
		_allStars.push_back(star);
	}
	
	void VisualisationData::prepare() {
		for(const Star *star : _allStars) {
			updateMaxMin(_maxPos, _minPos, star->position);
			updateMaxMin(_maxColor, _minColor, star->apparentColor);
			updateMaxMin(_maxAbsoluteMagnitude, _minAbsoluteMagnitude, star->absoluteMagnitude);
			updateMaxMin(_maxLuminosity, _minLuminosity, star->luminosity);
			updateMaxMin(_maxTemperature, _minTemperature, star->temperature);
			updateMaxMin(_maxRadius, _minRadius, star->radius);
		}
		
		_posNormFactor = 1.0f/(_maxPos - _minPos);
		_minAllPos = std::min(_minPos.x, std::min(_minPos.y, _minPos.z));
		_maxAllPos = std::max(_maxPos.x, std::max(_maxPos.y, _maxPos.z));
		_allPosNormFactor = 1.0f/(_maxAllPos - _minAllPos);
		_colorNormFactor = 1.0f/(_maxColor - _minColor);
		_abbsoluteMagnitudeNormFactor=  1.0/(_maxAbsoluteMagnitude - _minAbsoluteMagnitude);
		_radiusNormFactor=  1.0/(_maxRadius - _minRadius);
		_luminosityNormFactor = 1.0/(_maxLuminosity - _minLuminosity);
		_temperatureNormFactor = 1.0/(_maxTemperature - _minTemperature);
	}
}