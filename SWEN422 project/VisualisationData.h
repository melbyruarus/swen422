//
//  VisualisationData.h
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#ifndef __SWEN422_project__VisualisationData__
#define __SWEN422_project__VisualisationData__

#include "VisualisationLoader.h"
#include <vector>

namespace swen422 {
	class VisualisationData : public VisualisationLoader {
		std::vector<const Star *> _allStars;
		
		// Store max and mins
		glm::vec3 _maxPos = glm::vec3(-FLT_MAX);
		glm::vec3 _minPos = glm::vec3(FLT_MAX);
		glm::vec3 _posNormFactor;
		float _maxAllPos = -FLT_MAX;
		float _minAllPos = FLT_MAX;
		float _allPosNormFactor;
		glm::vec3 _maxColor = glm::vec3(-FLT_MAX);
		glm::vec3 _minColor = glm::vec3(FLT_MAX);
		glm::vec3 _colorNormFactor;
		float _maxAbsoluteMagnitude = -FLT_MAX;
		float _minAbsoluteMagnitude = FLT_MAX;
		float _abbsoluteMagnitudeNormFactor;
		float _maxRadius = -FLT_MAX;
		float _minRadius = FLT_MAX;
		float _radiusNormFactor;
		float _maxLuminosity = -FLT_MAX;
		float _minLuminosity = FLT_MAX;
		float _luminosityNormFactor;
		float _maxTemperature = -FLT_MAX;
		float _minTemperature = FLT_MAX;
		float _temperatureNormFactor;
	public:
		~VisualisationData() {
			for(const Star *s : _allStars) {
				delete s;
			}
		}
		
		// Override
		void addStar(const Star *);
		
		void prepare();
		
		const std::vector<const Star *> &stars() { return _allStars; }
		
		// Normalization
		float normalizeAbsoluteMagnitude(const float &mag) {
			return (mag - _minAbsoluteMagnitude) * _abbsoluteMagnitudeNormFactor;
		}
		
		float normalizeRadius(const float &rad) {
			return (rad - _minRadius) * _radiusNormFactor;
		}
		
		float normalizeLuminosity(const float &lum) {
			return (lum - _minLuminosity) * _luminosityNormFactor;
		}
		
		float normalizeTemperature(const float &temp) {
			return (temp - _minTemperature) * _temperatureNormFactor;
		}
		
		glm::vec3 normalizePosition(const glm::vec3 &pos) {
			return (pos - _minAllPos) * _allPosNormFactor;
		}
		
		float normalizePositionX(const float &pos) {
			return (pos - _minPos.x) * _posNormFactor.x;
		}
		
		float normalizePositionY(const float &pos) {
			return (pos - _minPos.y) * _posNormFactor.y;
		}
		
		float normalizePositionZ(const float &pos) {
			return (pos - _minPos.z) * _posNormFactor.z;
		}
		
		glm::vec3 normalizeColor(const glm::vec3 &color) {
			return (color - _minColor) * _colorNormFactor;
		}
		
		float normalizeColorR(const float &color) {
			return (color - _minColor.x) * _colorNormFactor.x;
		}
		
		float normalizeColorG(const float &color) {
			return (color - _minColor.y) * _colorNormFactor.y;
		}
		
		float normalizeColorB(const float &color) {
			return (color - _minColor.z) * _colorNormFactor.z;
		}
	};
}

#endif /* defined(__SWEN422_project__VisualisationData__) */
