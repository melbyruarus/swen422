//
//  CelestiaInterface.cpp
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#include "CelestiaInterface.h"

#include <fstream>
#include <iostream>
#include <string>
#include <celestia/celestiacore.h>
#include "glmutil.h"
#include <ILGL/Common/Logging.h>

using namespace std;

static void loadCrossIndex(StarDatabase* starDB,
                           StarDatabase::Catalog catalog,
                           const string& filename) {
    if(!filename.empty()) {
        ifstream xrefFile(filename.c_str(), ios::in | ios::binary);
        if(xrefFile.good()) {
            if(!starDB->loadCrossIndex(catalog, xrefFile)) {
                ILLogd("Error reading cross index %s", filename.c_str());
			}
            else {
                ILLogd("Loaded cross index %s", filename.c_str());
			}
        }
    }
}

bool CelestiaInterface::loadDatabase(swen422::VisualisationLoader *loader) {
	string starNamesFilePath = _resourceDir+"/starnames.dat";
	string starDatabaseFilePath = _resourceDir+"/stars.dat";
	string HDCrossIndexFilePath = _resourceDir+"/hdxindex.dat";
	string SAOCrossIndexFilePath = _resourceDir+"/saoxindex.dat";
	
	ifstream starNamesFile(starNamesFilePath.c_str(), ios::in);
    if (!starNamesFile.good()) {
		ILLoge("Error opening %s", starNamesFilePath.c_str());
        return false;
    }
	
    StarNameDatabase* starNameDB = StarNameDatabase::readNames(starNamesFile);
    if(starNameDB == NULL) {
        ILLoge("Error reading star names file");
        return false;
    }
	
    // First load the binary star database file.  The majority of stars
    // will be defined here.
    StarDatabase* starDB = new StarDatabase();
	ifstream starFile(starDatabaseFilePath.c_str(), ios::in | ios::binary);
	if(!starFile.good()) {
		ILLoge("Error opening %s", starDatabaseFilePath.c_str());
		delete starDB;
		return false;
	}
	
	if(!starDB->loadBinary(starFile)) {
		delete starDB;
		ILLoge("Error reading stars file");
		return false;
	}
	
    starDB->setNameDatabase(starNameDB);
	
    loadCrossIndex(starDB, StarDatabase::HenryDraper, HDCrossIndexFilePath);
    loadCrossIndex(starDB, StarDatabase::SAO,         SAOCrossIndexFilePath);
	
	starDB->finish();
	
	int count = 0;
	for(int n=0;n<starDB->size();n++) {
		Star *s = starDB->getStar(n);
		
		if(s && (count++)%2 == 0) {
			swen422::Star *outStar = new swen422::Star();
			outStar->absoluteMagnitude = s->getAbsoluteMagnitude();
			outStar->bolometricMagnitude = s->getBolometricMagnitude();
			outStar->luminosity = s->getLuminosity();
			outStar->radius = s->getRadius();
			outStar->spectralTypeName = s->getSpectralType();
			StellarClass c = StellarClass::parse(outStar->spectralTypeName);
			outStar->spectralClass = static_cast<swen422::SpectralClass>(c.getSpectralClass());
			outStar->subclass = c.getSubclass();
			outStar->luminosityClass = static_cast<swen422::LuminosityClass>(c.getLuminosityClass());
			outStar->type = static_cast<swen422::StarType>(c.getStarType());
			outStar->apparentColor = convert<float, Eigen::Matrix<float, 1, 3>>(c.getApparentColor().toVector3());
			outStar->temperature = s->getTemperature();
			outStar->position = convert<float, Eigen::Matrix<float, 1, 3>>(s->getPosition());
			outStar->name = starDB->getStarName(*s);
						
			loader->addStar(outStar);
		}
	}
	
	return true;
}
