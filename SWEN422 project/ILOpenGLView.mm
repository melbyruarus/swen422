//
//  ILOpenGLView.m
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#import "ILOpenGLView.h"

@implementation ILOpenGLView

- (void)awakeFromNib {
	GLint swapInt = 1;
	[[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
	
	_renderer = new swen422::VisualisationRenderer();
	
	[NSTimer scheduledTimerWithTimeInterval:1/swen422::VisualisationRenderer::kFPS
									 target:self
								   selector:@selector(setNeedsDisplay)
								   userInfo:nil
									repeats:YES];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(windowMove)
												 name:NSWindowDidMoveNotification
											   object:self.window];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[super dealloc];
}

- (void)windowMove {
	NSRect frame = [self.window convertRectToScreen:[self convertRect:self.bounds
															   toView:self.window.contentView]];
	
	_renderer->setViewOrigin(glm::vec2(frame.origin.x, frame.origin.y));
}

- (void)setNeedsDisplay {
	[self setNeedsDisplay:YES];
}

- (void)prepareOpenGL {
	if(glewInit() != GLEW_OK) {
		ILLoge("Unable to init glew");
		exit(-1);
	}
}

- (void)drawRect:(NSRect)dirtyRect {
	[[self openGLContext] makeCurrentContext];
	_renderer->render();
	[[self openGLContext] flushBuffer];
	
	ILCheckOpenGLError();
}

- (void)updateProjection {
	_renderer->updateProjection(self.bounds.size.width, self.bounds.size.height);
}

- (void)reshape {
	[super reshape];
	[[self openGLContext] makeCurrentContext];
	[self updateProjection];
	[self windowMove];
}

- (void)update {
	[super update];
	[[self openGLContext] makeCurrentContext];
	[self updateProjection];
	[self windowMove];
}

@synthesize renderer = _renderer;

@end
