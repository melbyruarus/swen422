//attribute vec4 il_Vertex;
//attribute vec4 il_Normal;
//attribute vec4 il_TexCoord0;
//attribute vec4 il_Color;

uniform float transitionPecentage;

void main(void) {
	vec4 newPos = gl_ProjectionMatrix * gl_ModelViewMatrix * vec4(gl_Normal, 1);
	vec4 oldPos = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_SecondaryColor;
	
	gl_Position = mix(newPos, oldPos, transitionPecentage);
	gl_FrontColor = mix(gl_Color, gl_Vertex, transitionPecentage);
	gl_FrontColor.a = 1.0;
	gl_PointSize = mix(gl_Color.a, gl_Vertex.a, transitionPecentage);
}
