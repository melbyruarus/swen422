//
//  VisualisationRenderer.cpp
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#include "VisualisationRenderer.h"

#include <GLUT/GLUT.h>
#include <fstream>
#include <ILGL/Common/Utilities.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <ILGL/Loading/OBJLoader.h>

using namespace ilgl::loading;

std::string names[] = { "Betelgeuse", "19 Psc", "OMI1 Cen", "The Garnet Star", "RHO Cas", "HIP 58954", "A Cen", "Antares", "Mimosa" };

namespace swen422 {
	const float VisualisationRenderer::kFrameDampningFactor = std::pow(kDampningFactor, 1/kFPS);
	const float VisualisationRenderer::kRotationAngleLowerInvCosThreshold = std::cos(kRotationAngleLowerThreshold);
	
	VisualisationRenderer::VisualisationRenderer() {
		_leapController.addListener(*this);
		resetViewport();
	}
	
	VisualisationRenderer::~VisualisationRenderer() {
		delete _pointVbo;
		delete _colorAndSizeVbo;
		delete _oldPointVbo;
		delete _oldColorAndSizeVbo;
		delete _starIndicies;
		
		delete _pointShader;
		delete _quadShader;
		_renderer->remove(_quad);
		delete _quad;
		
		delete _renderer;
		
		_leapController.removeListener(*this);
	}
	
	void VisualisationRenderer::onConnect(const Leap::Controller &controller) {
		ILLogd("Leap connected");
		
		if(controller.locatedScreens().count() == 0) {
			ILLoge("No screens found");
			return;
		}
		
		_mainScreen = controller.locatedScreens()[0];
	}
	
	template <typename T>
	T expAverage(const T &oldVal, const T &newVal, float distance, float weight) {
		float newWeight = 0.5;
		return oldVal * (1.0f-newWeight) + newVal;
	}
	
	void VisualisationRenderer::onFrame(const Leap::Controller& controller) {
		Leap::Frame frame = controller.frame();
		
		if(_mainScreen.isValid()) {
			_lastFrameHands = _hands;
			ilgl::timestamp_t currentTimestamp = ilgl::timestamp();
			
			const Leap::HandList &hands = frame.hands();
			for(const Leap::Hand &hand : hands) {
				// Find/Create a swen422 Hand
				bool handClosed = hand.fingers().count() <= 2;
				
				Hand *foundHand = NULL;
				for(Hand &h : _hands) {
					if(hand.id() == h.id()) {
						foundHand = &h;
						break;
					}
				}
				
				if(!foundHand) {
					_hands.push_back(Hand(hand.id()));
					foundHand = &*(_hands.end()-1);
					foundHand->isClosed = false;
					foundHand->lastUpdate = 0;
				}
				
				if(!foundHand->isClosed) {
					foundHand->modelViewMatrix = modelViewMatrix();
					foundHand->cameraPosition = cameraPosition();
				}
				
				// Calculate position of hand
				const Leap::Vector &palmPosition = hand.palmPosition();
				glm::vec3 handPosition = glm::vec3(glm::dvec4(palmPosition.x/400, palmPosition.y/400, palmPosition.z/400, 0) * _startGestureCameraRotation);
				
				// Update other hand info
				foundHand->lastFrameLength = currentTimestamp - foundHand->lastUpdate;
				if(foundHand->lastUpdate == 0) {
					foundHand->frameVelocity = glm::vec3(0);
				}
				else {
					foundHand->frameVelocity = (handPosition - foundHand->position) / foundHand->lastFrameLength;
				}
				foundHand->position = handPosition;
				foundHand->isClosed = handClosed;
				foundHand->lastUpdate = currentTimestamp;
			}
			
			auto it = _hands.begin();
			while(it != _hands.end()) {
				bool stillExists = false;
				
				for(const Leap::Hand &hand : hands) {
					if((*it).id() == hand.id()) {
						stillExists = true;
						break;
					}
				}
				
				if(!stillExists) {
					it = _hands.erase(it);
				}
				else {
					it++;
				}
			}
			
			processGesture();
		}
	}
	
	void VisualisationRenderer::processGesture() {
		// TODO: implement continuation of 2 hand momentum & 2 hand movement
		
		std::vector<const Hand *> closedHands;
		
		for(const Hand &hand : _hands) {
			if(hand.isClosed) {
				closedHands.push_back(&hand);
			}
		}
		
		if(closedHands.size() != _closedHandsLastFrame || closedHands.size() == 0) {
			_momentuming = false;
			_momentumVelocity = glm::vec3(0);
			
			_startGestureCameraCenter = _cameraCenter;
			_startGestureCameraRotation = _cameraRotation;
			_startGestureCameraDistance = _cameraDistance;
			_startGestureCameraMatrix = cameraMatrix();
			
			for(Hand &hand : _hands) {
				hand.gestureStartPosition = hand.position;
			}
		}
		
		if((int)closedHands.size() < _closedHandsLastFrame) {
			_closedHandsLastFrame = -1;
			return;
		}
		
		if(closedHands.size() == 1) {
			const Hand *hand = closedHands[0];
									
			_cameraCenter = _startGestureCameraCenter - (hand->position - hand->gestureStartPosition) * _cameraDistance;
			_cameraCenter.x = glm::clamp(_cameraCenter.x, 0.0f, 1.0f);
			_cameraCenter.y = glm::clamp(_cameraCenter.y, 0.0f, 1.0f);
			_cameraCenter.z = glm::clamp(_cameraCenter.z, 0.0f, 1.0f);
			_translationVelocity = expAverage(_translationVelocity, hand->frameVelocity, hand->lastFrameLength, 1)*0.01f;
			
			updateModelView();
		}
		else if(closedHands.size() > 1) {			
			const Hand *hand1 = closedHands[0];
			const Hand *hand2 = closedHands[1];
			
			glm::dvec3 p1old = glm::dvec3(hand1->gestureStartPosition);
			glm::dvec3 p1new = glm::dvec3(hand1->position);
			glm::dvec3 p2old = glm::dvec3(hand2->gestureStartPosition);
			glm::dvec3 p2new = glm::dvec3(hand2->position);
						
			glm::dvec3 vold = p2old - p1old;
			glm::dvec3 vnew = p2new - p1new;
						
			glm::dvec3 up;
			double dot = glm::dot(glm::normalize(vold), glm::normalize(vnew));
			double angle;
			double radAngle;
			
			if(dot < 1) {
				radAngle = acos(dot);
			}
			else {
				radAngle = 0;
			}
			angle = radAngle/M_PI*180;
						
			if(angle > kRotationAngleLowerThreshold) {
				up = glm::normalize(glm::cross(vold, vnew));
			}
			else {
				up = glm::vec3(0, 0, 1);
			}
									
			_cameraDistance = glm::clamp(_startGestureCameraDistance * (float)(glm::length(vold)/glm::length(vnew)), 0.0005f, 1.0f);
			glm::dvec3 axis = up * sin(radAngle / 2);
			float scalar = cosf(radAngle / 2);
			_cameraRotation = _startGestureCameraRotation * glm::dquat(scalar, axis.x, axis.y, axis.z);

			updateModelView();
		}
		
		_closedHandsLastFrame = closedHands.size();
	}
	
	glm::vec3 VisualisationRenderer::cameraPosition() const {
		// This works (tested)
		return	glm::vec3(glm::dvec3(0, 0, _cameraDistance) * _cameraRotation) + _cameraCenter;
	}
	
	glm::dmat4 VisualisationRenderer::cameraMatrix() const {
		return	glm::translate(glm::dvec3(0, 0, -_cameraDistance)) *
				glm::mat4_cast(_cameraRotation) *
				glm::translate(glm::dvec3(-_cameraCenter));
	}
	
	glm::dmat4 VisualisationRenderer::modelViewMatrix() const {
		return	glm::scale((double)kWorldScaleFactor, (double)kWorldScaleFactor, (double)kWorldScaleFactor) *
				cameraMatrix();
	}
	
	void VisualisationRenderer::glInit() {
		glClearColor(0, 0, 0, 1);
		glEnable(GL_POINT_SMOOTH);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		
		_renderer = new Renderer();
		_renderer->glSetup();
		
		AbstractResource *qin = _resourceDir->get("quad.obj");
		assert(qin->isValid());
		assert(!qin->isDirectory());
		std::vector<OBJMesh *> *meshes = ilgl::loading::OBJLoader::parseOBJ(*dynamic_cast<ResourcePointer *>(qin));
		delete qin;
		assert(meshes->size() == 1);
		OBJMesh *mesh = (*meshes)[0];
		_quad = _renderer->add(mesh);
		_quad->glInitIfNeeded();
//		for(OBJMesh *m : *meshes) {
//			delete m;
//		}
//		delete meshes;
		
		AbstractResource *sin = _resourceDir->get("sphere.obj");
		assert(sin->isValid());
		assert(!sin->isDirectory());
		meshes = ilgl::loading::OBJLoader::parseOBJ(*dynamic_cast<ResourcePointer *>(sin));
		delete sin;
		assert(meshes->size() == 1);
		mesh = (*meshes)[0];
		_sphere = _renderer->add(mesh);
		_sphere->glInitIfNeeded();
//		for(OBJMesh *m : *meshes) {
//			delete m;
//		}
//		delete meshes;
				
		AbstractResource *psin = _resourceDir->get("pointshader.vs");
		assert(psin->isValid());
		assert(!psin->isDirectory());
		Shader *psVShader = new Shader(ShaderType::Vertex, *dynamic_cast<ResourcePointer *>(psin));
		delete psin;
		_pointShader = new ShaderProgram(psVShader);
		_trasitioningPercentageUniformLocation = _pointShader->uniformLocation("transitionPecentage");
		delete psVShader;
		
		AbstractResource *qsin = _resourceDir->get("quadshader.vs");
		assert(qsin->isValid());
		assert(!qsin->isDirectory());
		Shader *qsVShader = new Shader(ShaderType::Vertex, *dynamic_cast<ResourcePointer *>(qsin));
		delete qsin;
		_quadShader = new ShaderProgram(qsVShader);
		delete qsVShader;
		
		updateModelView();
	}
	
	void VisualisationRenderer::updateModelView() {
		_modelViewMatrix = modelViewMatrix();
	}
	
	void VisualisationRenderer::updateProjection(const int &width, const int &height) {
		_viewSize = glm::vec2(width, height);
		
		glViewport(0, 0, width, height);
		_projectionMatrix = glm::perspective(80.0, (double)width/(double)height, (double)kZNear, (double)kZFar);
		
		applyPerspectiveMatrix();
	}
	
	void VisualisationRenderer::applyPerspectiveMatrix() const {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glLoadMatrixd(glm::value_ptr(_projectionMatrix));
		glMatrixMode(GL_MODELVIEW);
		
		ILCheckOpenGLError();
	}
	
	void VisualisationRenderer::applyOrthoMatrix() const {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glLoadMatrixd(glm::value_ptr(glm::ortho(0.0, (double)_viewSize.x, 0.0, (double)_viewSize.y, 0.5, 100.0)));
		glMatrixMode(GL_MODELVIEW);
		
		ILCheckOpenGLError();
	}
	
	inline static float sizeFor(const float &size) {
		return std::max(2.0f, size*32);
	}
	
	void VisualisationRenderer::getVBOData(std::vector<float> &points, std::vector<float> &colorsAndSize) {
		for(Node *n : _visualisation->nodes()) {
			points.push_back(n->position.x);
			points.push_back(n->position.y);
			points.push_back(n->position.z);
			
			colorsAndSize.push_back(n->color.r);
			colorsAndSize.push_back(n->color.g);
			colorsAndSize.push_back(n->color.b);
			colorsAndSize.push_back(sizeFor(n->size));
		}
	}
	
	void VisualisationRenderer::setupVBO() {
		std::clock_t start = std::clock();
		
		std::vector<float> points;
		std::vector<float> colorsAndSize;
		std::vector<ilgl::mesh_index_t> indicies;
		
		getVBOData(points, colorsAndSize);
		
		for(int n=0;n<points.size()/3;n++) {
			indicies.push_back(n);
		}
		
		if(_starIndicies) {
			delete _starIndicies;
		}
		_starIndicies = new VBO(_renderer, AttributeType::Index, GLTypeFromArrayType<typeof(indicies)>::value, &indicies[0], indicies.size(), 1, 0, VboType::ElementArrayBuffer);
		_starIndicies->glInitIfNeeded();
		
		if(_pointVbo) {
			delete _pointVbo;
		}
		_pointVbo = new VBO(_renderer, AttributeType::Normal, GLTypeFromArrayType<typeof(points)>::value, &points[0], points.size()/3, 3);
		_pointVbo->glInitIfNeeded();
		
		if(_colorAndSizeVbo) {
			delete _colorAndSizeVbo;
		}
		_colorAndSizeVbo = new VBO(_renderer, AttributeType::Color, GLTypeFromArrayType<typeof(colorsAndSize)>::value, &colorsAndSize[0], colorsAndSize.size()/4, 4);
		_colorAndSizeVbo->glInitIfNeeded();
		
		if(_oldPointVbo) {
			delete _oldPointVbo;
		}
		_oldPointVbo = new VBO(_renderer, AttributeType::SecondaryColor, GLTypeFromArrayType<typeof(_oldPoints)>::value, &_oldPoints[0], _oldPoints.size()/3, 3);
		_oldPointVbo->glInitIfNeeded();
		
		if(_oldColorAndSizeVbo) {
			delete _oldColorAndSizeVbo;
		}
		_oldColorAndSizeVbo = new VBO(_renderer, AttributeType::Vertex, GLTypeFromArrayType<typeof(_oldColorsAndSize)>::value, &_oldColorsAndSize[0], _oldColorsAndSize.size()/4, 4);
		_oldColorAndSizeVbo->glInitIfNeeded();
				
		ILLogd("Uploading VBOs took: %f seconds", (float)(std::clock() - start)/(float)CLOCKS_PER_SEC);
		
		ILCheckOpenGLError();
	}
	
	static const float fontWidth = 8;
	static void * font = GLUT_BITMAP_8_BY_13;
	void drawtext(const char *message, int posx, int posy, float color) {
		glPushMatrix();
		glLoadIdentity();
		glEnable(GL_BLEND);
		glTranslatef(0, 0, -0.6);
		glColor4f(1, 1, 1, color);
		
		glRasterPos2f(posx, posy);
		while(*message) {
			glutBitmapCharacter(font, *message++);
		}
		
		glDisable(GL_BLEND);
		glPopMatrix();
	}
	
	void drawtext(const std::string &s, int posx, int posy, float color) {
		drawtext(s.c_str(), posx, posy, color);
	}
	
	std::string textFor(AxisOption::Type option) {
		switch(option) {
			case AxisOption::PositionX:
				return "Position X";
			case AxisOption::PositionY:
				return "Position Y";
			case AxisOption::PositionZ:
				return "Position Z";
			case AxisOption::Luminosity:
				return "Luminosity";
			case AxisOption::Magnitude:
				return "Magnitude";
			case AxisOption::Radius:
				return "Radius";
			case AxisOption::Temperature:
				return "Temperature";
			case AxisOption::ColorR:
				return "Color Red";
			case AxisOption::ColorG:
				return "Color Green";
			case AxisOption::ColorB:
				return "Color Blue";
		}
	}
	
	void format_commas(float n, int decimals, char *out) {
		int c;
		char buf[50];
		char *p;
		
		sprintf(buf, "%0.*f", decimals, n);
		int len = strlen(buf);
		for(int i=0;i<len;i++) {
			if(buf[i] == '.') {
				len = i;
				break;
			}
		}
		c = 2 - (len) % 3;
		bool afterDecimal = false;
		for(p = buf; *p != '\0'; p++) {
			*out++ = *p;
			if(*(p+1) == '.') {
				afterDecimal = true;
			}
			if(c == 1 && !afterDecimal) {
				*out++ = ',';
			}
			c = (c + 1) % 3;
		}
		*--out = 0;
	}
		
	void VisualisationRenderer::render() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		updateModelView();
		glLoadMatrixd(glm::value_ptr(_modelViewMatrix));
				
		ILCheckOpenGLError();
		
		if(_visualisation) {
			if(!_renderer) {
				glInit();
			}
			
			if(_needVBOUpdate) {
				setupVBO();
				_needVBOUpdate = false;
			}
			
			// Draw sphere and axes
			glPushMatrix();
			glDepthMask(GL_FALSE);
			glEnable(GL_BLEND);
			glTranslatef(_cameraCenter.x, _cameraCenter.y, _cameraCenter.z);
			for(float scale=0.1, color=1;scale >= 0.0001;scale/=9.999, color*=0.8) {
				glColor4f(0, 1-color, color, 0.2);
				glPushMatrix();
				glScaled(scale, scale, scale);
				_sphere->draw();
				glPopMatrix();
			}
			glDisable(GL_BLEND);
			glDepthMask(GL_TRUE);
			glPopMatrix();
			
			float axisXMin = _cameraCenter.x - 0.5;
			float axisXMax = _cameraCenter.x + 0.5;
			float axisYMin = _cameraCenter.y - 0.5;
			float axisYMax = _cameraCenter.y + 0.5;
			float axisZMin = _cameraCenter.z - 0.5;
			float axisZMax = _cameraCenter.z + 0.5;
			
			glBegin(GL_LINES);
			glColor3f(1, 0, 0);
			glVertex3f(axisXMin, _cameraCenter.y, _cameraCenter.z);
			glVertex3f(axisXMax, _cameraCenter.y, _cameraCenter.z);
			glEnd();
			
			glBegin(GL_LINES);
			glColor3f(0, 1, 0);
			glVertex3f(_cameraCenter.x, axisYMin, _cameraCenter.z);
			glVertex3f(_cameraCenter.x, axisYMax, _cameraCenter.z);
			glEnd();
			
			glBegin(GL_LINES);
			glColor3f(0, 0, 1);
			glVertex3f(_cameraCenter.x, _cameraCenter.y, axisZMin);
			glVertex3f(_cameraCenter.x, _cameraCenter.y, axisZMax);
			glEnd();
			
			glPointSize(16);
			glColor3f(0, 0, 1);
			glDepthFunc(GL_ALWAYS);
			glBegin(GL_POINTS);
			glVertex3f(_cameraCenter.x, _cameraCenter.y, _cameraCenter.z);
			glEnd();
			glDepthFunc(GL_LEQUAL);
			
			// Draw point cloud
			
			glEnable(GL_PROGRAM_POINT_SIZE);
			_pointShader->enable();
			if(_transitioning) {
				_transitionFrame++;
				if(_transitionFrame >= 30) {
					_transitioning = 0;
				}
			}
			_trasitioningPercentageUniformLocation->setValue(1.0f - _transitionFrame / 30.0f);
			
			_pointVbo->enable();
			_pointVbo->bind();
			
			_oldPointVbo->enable();
			_oldPointVbo->bind();
			
			_colorAndSizeVbo->enable();
			_colorAndSizeVbo->bind();
			
			_oldColorAndSizeVbo->enable();
			_oldColorAndSizeVbo->bind();

			_starIndicies->enable();
			_starIndicies->bind();
			
			glDrawElements(GL_POINTS,					// mode
						   _starIndicies->count(),		// count
						   _starIndicies->dataType(),	// type
						   (void*)0						// element array buffer offset
						   );
			
			_starIndicies->unbind();
			_starIndicies->disable();
			
			_oldColorAndSizeVbo->unbind();
			_oldColorAndSizeVbo->disable();
			
			_colorAndSizeVbo->unbind();
			_colorAndSizeVbo->disable();
			
			_oldPointVbo->unbind();
			_oldPointVbo->disable();
			
			_pointVbo->unbind();
			_pointVbo->disable();
						
			_pointShader->disable();
			glDisable(GL_PROGRAM_POINT_SIZE);

			ILCheckOpenGLError();
			
			// User testing
			
			if(_selectedStar) {
				glPointSize(sizeFor(_selectedStar->size)+3);
				glBegin(GL_POINTS);
				glColor3f(0,1,0);
				glVertex3fv(glm::value_ptr(_selectedStar->position));
				glEnd();
				ILCheckOpenGLError();
			}
			
			ILCheckOpenGLError();
			
			// Draw text
			
			applyOrthoMatrix();
			glLoadIdentity();
			updateModelView();
			
			// Draw axes labels
			
			bool vis;
			glm::ivec2 left = ndcToScreen(project(glm::dvec4(axisXMin, _cameraCenter.y, _cameraCenter.z, 1), &vis));
			if(vis) {
				drawtext("- "+textFor(_visualisation->xAxis()), left.x, left.y, 1);
			}
			glm::ivec2 right = ndcToScreen(project(glm::dvec4(axisXMax, _cameraCenter.y, _cameraCenter.z, 1), &vis));
			if(vis) {
				drawtext("+ "+textFor(_visualisation->xAxis()), right.x, right.y, 1);
			}
			glm::ivec2 top = ndcToScreen(project(glm::dvec4(_cameraCenter.x, axisYMin, _cameraCenter.z, 1), &vis));
			if(vis) {
				drawtext("- "+textFor(_visualisation->yAxis()), top.x, top.y, 1);
			}
			glm::ivec2 bottom = ndcToScreen(project(glm::dvec4(_cameraCenter.x, axisYMax, _cameraCenter.z, 1), &vis));
			if(vis) {
				drawtext("+ "+textFor(_visualisation->yAxis()), bottom.x, bottom.y, 1);
			}
			glm::ivec2 front = ndcToScreen(project(glm::dvec4(_cameraCenter.x, _cameraCenter.y, axisZMin, 1), &vis));
			if(vis) {
				drawtext("- "+textFor(_visualisation->zAxis()), front.x, front.y, 1);
			}
			glm::ivec2 back = ndcToScreen(project(glm::dvec4(_cameraCenter.x, _cameraCenter.y, axisZMax, 1), &vis));
			if(vis) {
				drawtext("+ "+textFor(_visualisation->zAxis()), back.x, back.y, 1);
			}
			
			// Draw labels
			
			if(_displayLabels) {
				static const int kClosest = 20;
				std::vector<Node *> closeNodes;
				std::vector<float> distances;
				_visualisation->getClosestNodes(_cameraCenter, kClosest, closeNodes, distances);
				float maxDist = distances[kClosest-1];

				for(int i=0;i<kClosest;i++) {
					Node *n = closeNodes[i];
					float distance = distances[i];
					float normalizedDistance = distance / maxDist;
									
					bool vis;
					glm::dvec4 screenPos = project(glm::dvec4(n->position, 1), &vis);
					if(vis) {
						glm::ivec2 c = ndcToScreen(screenPos);
						
						char tmp[50];
						char luminosity[100];
						format_commas(n->star->luminosity, 2, tmp);
						sprintf(luminosity, "Luminosity: %s x sun", tmp);
						char temp[100];
						format_commas(n->star->temperature, 0, tmp);
						sprintf(temp, "Temperature: %sK", tmp);
						char radius[100];
						format_commas(n->star->radius, 0, tmp);
						sprintf(radius, "Radius: %sKm", tmp);
						
						std::vector<std::string> dataToShow;
						dataToShow.push_back(n->star->name);
						dataToShow.push_back("Class: "+n->star->spectralTypeName);
						dataToShow.push_back(luminosity);
						dataToShow.push_back(temp);
						dataToShow.push_back(radius);
						
						int count = 0;
						float divToRad = (dataToShow.size() - 1) / M_PI;
						float rad = std::max(n->size, 30.0f);
						for(std::string &str : dataToShow) {
							float angle = count/divToRad;
							int ox = rad * sin(angle);
							int oy = rad * ((-2 * count / ((float)dataToShow.size() - 1) + 1));
							
							int x = c.x + ox;
							int y = c.y + oy;
							
							drawtext(str.c_str(), x, y, sqrtf(1-normalizedDistance));
							
							count++;
						}
					}
				}
			}
			
			applyPerspectiveMatrix();
			
			ILCheckOpenGLError();
		}
	}
	
	glm::dvec4 VisualisationRenderer::project(glm::dvec4 point, bool *isVisible) const {
		glm::dvec4 screenPos = _projectionMatrix * _modelViewMatrix * point;
		if(isVisible) {
			*isVisible = screenPos.w > 0;
		}
		screenPos /= screenPos.w;
		if(isVisible) {
			*isVisible |= screenPos.x > 1 || screenPos.x < -1 || screenPos.y > 1 || screenPos.y < -1;
		}
		
		return screenPos;
	}
	
	glm::ivec2 VisualisationRenderer::ndcToScreen(glm::dvec4 ndc) const {
		return glm::ivec2((ndc.x / 2.0 + 0.5) * _viewSize.x,
						  (ndc.y / 2.0 + 0.5) * _viewSize.y);
	}
	
	void VisualisationRenderer::drawQuad(double x, double y, double z, double w, double h) {
		glLoadMatrixd(glm::value_ptr(glm::translate(x, y, -0.7 - (z / 100)) * glm::scale(w, h, 1.0)));
		_quad->draw();
	}
	
	void VisualisationRenderer::prepareVisualisationUpdate() {
		_oldPoints.clear();
		_oldColorsAndSize.clear();
		getVBOData(_oldPoints, _oldColorsAndSize);
	}
	
	void VisualisationRenderer::updateVisualisation() {
		_needVBOUpdate = true;
		_transitioning = true;
		_transitionFrame = 0;
	}
	
	void VisualisationRenderer::resetViewport() {
		_cameraCenter = glm::vec3(0.5, 0.5, 0.5);
		_cameraDistance = 1;
		_cameraRotation = glm::dquat(1, 0, 0, 0);
	}
	
	void VisualisationRenderer::setHighlightedStar(int o) {
		if(o < 0) {
			_selectedStar = NULL;
		}
		else {
			_selectedStar = _visualisation->nodeWithName(names[o]);
		}
	}
	
	void VisualisationRenderer::setSetDisplayLabels(bool display) {
		_displayLabels = display;
	}
}