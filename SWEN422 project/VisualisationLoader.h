//
//  VisualisationLoader.h
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#ifndef __SWEN422_project__VisualisationLoader__
#define __SWEN422_project__VisualisationLoader__

#include "Star.h"

namespace swen422 {
	class VisualisationLoader {
	public:
		virtual void addStar(const Star *) = 0;
	};
}

#endif /* defined(__SWEN422_project__VisualisationLoader__) */
