//
//  Star.cpp
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#include "Star.h"

namespace swen422 {
	std::ostream &operator<<(std::ostream &out, const Star &s) {
		out << '(' << s.name << " pos: " << glm::to_string(s.position) << " absmag: " << s.absoluteMagnitude <<
		" bolmag: " << s.bolometricMagnitude << " lum: " << s.luminosity << " radius: " << s.radius <<
		" temp: " << s.temperature << " spec: " << s.spectralClass << ':' << s.subclass << " (" << s.spectralTypeName <<
		") lumc: " << s.luminosityClass << " type: " << s.type << " apcolor: " << glm::to_string(s.apparentColor) << ')';
		return out;
	}
}