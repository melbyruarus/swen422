//
//  ILAppDelegate.m
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#import "ILAppDelegate.h"

#import "CelestiaInterface.h"
#import "VisualisationData.h"
#import "Visualisation.h"
#import "VisualisationRenderer.h"
#import "ILOpenGLView.h"
#import <ILGL/Resources/FSPointer.h>

swen422::Visualisation *vis = NULL;

@implementation ILAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
	CelestiaInterface *celInterface = new CelestiaInterface([[[NSBundle mainBundle] resourcePath] UTF8String]);
	swen422::VisualisationData *visdata = new swen422::VisualisationData();
	
	celInterface->loadDatabase(visdata);
	if(!visdata) {
		exit(-1);
	}
	
	delete celInterface;
	celInterface = NULL;
	
	ILLogd("Loaded database");
	
	visdata->prepare();
	
	ILLogd("Prepared the visualisation data");
	
	vis = new swen422::Visualisation(visdata);
	
	ILLogd("Created the visualisation");
	
	vis->resetToDefault();
	
	ILLogd("Intilized the visualisation");
	
	[_renderView renderer]->setResourceDir(new FSDirectory([[[NSBundle mainBundle] resourcePath] UTF8String]));
	[_renderView renderer]->setVisualisation(vis);
	[_renderView renderer]->prepareVisualisationUpdate();
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)xAxisChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->setXAxis((swen422::AxisOption::Type)sender.selectedTag);
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)xAxisScaleChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->setXAxisScale((swen422::AxisScale::Type)sender.selectedTag);
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)yAxisChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->setYAxis((swen422::AxisOption::Type)sender.selectedTag);
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)yAxisScaleChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->setYAxisScale((swen422::AxisScale::Type)sender.selectedTag);
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)zAxisChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->setZAxis((swen422::AxisOption::Type)sender.selectedTag);
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)zAxisScaleChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->setZAxisScale((swen422::AxisScale::Type)sender.selectedTag);
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)sizeAxisChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->setSizeAxis((swen422::AxisOption::Type)sender.selectedTag);
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)sizeAxisScaleChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->setSizeAxisScale((swen422::AxisScale::Type)sender.selectedTag);
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)colorAxisChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->setColorAxis((swen422::ColorOption::Type)sender.selectedTag);
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)colorAxisScaleChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->setColorAxisScale((swen422::AxisScale::Type)sender.selectedTag);
	[_renderView renderer]->updateVisualisation();
}

- (IBAction)resetVisualisation:(id)sender {
	[_renderView renderer]->prepareVisualisationUpdate();
	vis->resetToDefault();
	[_renderView renderer]->updateVisualisation();
	[self updateUI];
}

- (IBAction)resetViewport:(id)sender {
	[_renderView renderer]->resetViewport();
}

- (void)updateUI {
	[xAxis selectItemWithTag:vis->xAxis()];
	[yAxis selectItemWithTag:vis->yAxis()];
	[zAxis selectItemWithTag:vis->zAxis()];
	[sizeAxis selectItemWithTag:vis->sizeAxis()];
	[colorAxis selectItemWithTag:vis->colorAxis()];
	
	[xScale selectItemWithTag:vis->xAxisScale()];
	[yScale selectItemWithTag:vis->yAxisScale()];
	[zScale selectItemWithTag:vis->zAxisScale()];
	[sizeScale selectItemWithTag:vis->sizeAxisScale()];
	[colorScale selectItemWithTag:vis->colorAxisScale()];
}

- (IBAction)selectedStarChanged:(NSPopUpButton *)sender {
	[_renderView renderer]->setHighlightedStar(sender.selectedTag);
}

- (IBAction)displayLabelsChanged:(NSButton *)sender {
	[_renderView renderer]->setSetDisplayLabels([sender state] == NSOnState);
}

@synthesize window = _window;

@end
