//
//  ILAppDelegate.h
//  SWEN422 project
//
//  Created by Melby Ruarus on 25/04/13.
//  Copyright (c) 2013 Liridesce. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class ILOpenGLView;

@interface ILAppDelegate : NSObject <NSApplicationDelegate> {
	NSWindow *_window;
	IBOutlet ILOpenGLView *_renderView;
	IBOutlet NSPopUpButton *xAxis;
	IBOutlet NSPopUpButton *xScale;
	IBOutlet NSPopUpButton *yAxis;
	IBOutlet NSPopUpButton *yScale;
	IBOutlet NSPopUpButton *zAxis;
	IBOutlet NSPopUpButton *zScale;
	IBOutlet NSPopUpButton *sizeAxis;
	IBOutlet NSPopUpButton *sizeScale;
	IBOutlet NSPopUpButton *colorAxis;
	IBOutlet NSPopUpButton *colorScale;
}

@property (assign) IBOutlet NSWindow *window;

@end
